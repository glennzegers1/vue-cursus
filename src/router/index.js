import Vue from 'vue'
import VueRouter from 'vue-router'
import ParticipantsList from '../views/ParticipantsList.vue'
import Participant from '../views/Participant.vue'
import AddPhoto from '../views/AddPhoto.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueRouter)
Vue.use(VueAxios, axios)

const routes = [
  {
    path: '/',
    name: 'participants-list',
    component: ParticipantsList
  },
  {
    path: '/photo',
    name: 'add-photo',
    component: AddPhoto
  },
  {
    path: '/participant/:id',
    name: 'participant',
    component: Participant
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
  